require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


import App from './vue/app'
import TaskForm from './vue/components/TaskForm'
import TaskList from "./vue/components/TaskList";
import PrerequisiteForm from "./vue/components/PrerequisiteForm";


import axios from "axios";
const router = new VueRouter({
    routes: [
        {
            path: '/tasks',
            name: 'tasks',
            component: TaskList
        },
        {
            path: '/create-task',
            name: 'createTask',
            component: TaskForm
        },
        {
            path: '/add-prerequisite',
            name: 'addPrerequisite',
            component: PrerequisiteForm
        }
    ]
})

Vue.mixin({
    methods: {
        async getTasks(){
            const data = await axios.get('/api/tasks').then(
                (res) => {
                     return res.data
                }
            )
            return data
        }
    }
})
const app = new Vue({
    el: '#app',
    components: { App },
    router

})
