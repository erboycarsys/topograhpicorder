## Kurulum

Projeyi indirdikten sonra, composer install, npm install yapın.

https://www.mongodb.com/compatibility/mongodb-laravel-intergration

Adresindeki yönergeleri izleyerek mongodb laravel kurulumunu gerçekleştirin.


Daha sonra config/database.php 95. satırında kullanacağınız mongodb adresini tanımlayın.


Projenin odağı topografik sıralama olduğundan, env dosyasında değişken tutmadım.


Herşey tamamlandıktan sonra, npm run dev, npm run watch vs ile çalıştırabilirsiniz.


topografik sıralama sayfası dışındaki proje vue ile spa dır.

topografik sıralama sonuç doğrudan ekrana basılmaktadır.

api dökümantasyonu openapi standardındadır ve otomaitk üretilmektedir.
yine projenin hedefi topografik sıralama olduğu için annotation kullanarak vakit kaybetmedim.


localhost:8000/docs/  ile dökümantasyona erişebilirsiniz.

localhost:8000 projeyi açar.


hoşça kalın. 

Yiğit.
