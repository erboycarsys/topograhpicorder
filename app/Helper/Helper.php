<?php

namespace App\Helper;

class Helper
{
    //this is simple depth search order ( topograhpic ordering )
    public static function sortDeps($items) {
        $res = array();
        $doneList = array();


        while(count($items) > count($res)) {
            $doneSomething = false;

            foreach($items as $itemIndex => $item) {
                if(isset($doneList[$item['title']])) {
                    continue;
                }
                $resolved = true;

                if(isset($item['prerequisites'])) {
                    foreach($item['prerequisites'] as $dep) {
                        if(!isset($doneList[$dep])) {
                            //cycle
                            $resolved = false;
                            break;
                        }
                    }
                }
                if($resolved) {
                    //all dependencies are met:
                    $doneList[$item['title']] = true;
                    $res[] = $item;
                    $doneSomething = true;
                }
            }
            if(!$doneSomething) {
                return "Dependency Cycling";
            }
        }
        return $res;
    }


}
