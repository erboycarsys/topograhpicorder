<?php

namespace App\Http\Controllers;

use App\Models\Task;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Mockery\Exception;
use App\Helper\Helper;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Task::all();
    }


    public function store(Request $request)
    {
        $data = $request->json()->all();

        $task = Task::where('title', $data['title'])->first();
        if ($task) {
            $response = ["status" => "This task has already been created", "code" => "0001"];
            return response()->json( $response);
        }
        $task = new Task();
        $task->fill($request->json()->all());
        $task->save();
        $response = ["status" => "success", "code" => "0000"];
        return response()->json( $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Task::where("_id", $id)->first();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function addPrerequisite(Request $request) {
        $data =  $request->json()->all();
        $task = Task::where('title', $data['task'])->first();
        $task->prerequisites = $data["prerequisites"];
        $task->save();
        $response = ["status" => "success", "code" => "0000"];
        return response()->json( $response);
    }


    public function orderTasks()
    {
        $tasks = Task::all();

        $sortedItems = Helper::sortDeps($tasks);
        return $sortedItems;
    }
}
